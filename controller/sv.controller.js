renderDSSV = function (dssv) {
  var contentHTML = "";

  dssv.forEach((sv) => {
    var diemTB = (sv.math * 1 + sv.physics * 1 + sv.chemistry * 1) / 3;
    var contentTr = `<tr>
    <td>${sv.id}</td>
    <td>${sv.name}</td>
    <td>${sv.email}</td>
    <td>${diemTB}</td>
    <td>
    <button onclick=xoaSinhVien('${sv.id}') class="btn btn-danger">Xoa</button> 
    <button onclick=suaSinhVien('${sv.id}') class="btn btn-warning">Sua</button> 
    </td>
    </tr>`;
    contentHTML += contentTr;
  });

  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
};

function batLoading() {
  document.getElementById("loading").style.display = "flex";
}

function tatLoading() {
  document.getElementById("loading").style.display = "none";
}

function showThongTinLenForm(sv) {
  document.getElementById("txtMaSV").value = sv.id;
  document.getElementById("txtTenSV").value = sv.name;
  document.getElementById("txtEmail").value = sv.email;
  document.getElementById("txtPass").value = sv.password;
  document.getElementById("txtDiemToan").value = sv.math;
  document.getElementById("txtDiemLy").value = sv.physics;
  document.getElementById("txtDiemHoa").value = sv.chemistry;
}

function layThongTinTuForm() {
  let newSV = {
    id: document.getElementById("txtMaSV").value,
    name: document.getElementById("txtTenSV").value,
    email: document.getElementById("txtEmail").value,
    password: document.getElementById("txtPass").value,
    math: document.getElementById("txtDiemToan").value,
    physics: document.getElementById("txtDiemLy").value,
    chemistry: document.getElementById("txtDiemHoa").value,
  };
  return newSV;
}

function clearForm() {
  document.getElementById("txtMaSV").value = "";
  document.getElementById("txtTenSV").value = "";
  document.getElementById("txtEmail").value = "";
  document.getElementById("txtPass").value = "";
  document.getElementById("txtDiemToan").value = "";
  document.getElementById("txtDiemLy").value = "";
  document.getElementById("txtDiemHoa").value = "";
  document.getElementById("txtMaSV").removeAttribute("disabled");
}

// function timKiemViTriTheoTen(name, dssv) {
//   for (var index = 0; index < dssv.length; index++) {
//     var sv = dssv[index];
//     if (sv.name == name) {
//       return index;
//     }
//   }
//   //  ko tìm thấy
//   return -1;
// }

function timKiemViTriTheoTen(name, dssv) {
  var newSvArr = [];
  for (var index = 0; index < dssv.length; index++) {
    var sv = dssv[index];
    if (sv.name == name) {
      newSvArr.push(sv);
    }
  }
  return newSvArr;
}
