const BASE_URL = "https://62db6ca4e56f6d82a7728511.mockapi.io";

function getDSSV() {
  batLoading();
  axios({
    url: `${BASE_URL}/sv`,
    method: "GET",
  })
    .then(function (res) {
      tatLoading();

      renderDSSV(res.data);
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
}
getDSSV();

function xoaSinhVien(id) {
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      getDSSV();
    })
    .catch(function (err) {
      console.log(err);
    });
}

function themSV() {
  let newSV = layThongTinTuForm();
  batLoading();
  axios({
    url: `${BASE_URL}/sv`,
    method: "POST",
    data: newSV,
  })
    .then(function (res) {
      tatLoading();
      getDSSV();
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
}

function suaSinhVien(id) {
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "GET",
  })
    .then(function (res) {
      showThongTinLenForm(res.data);
      document.getElementById("txtMaSV").setAttribute("disabled", "");
    })
    .catch(function (err) {
      console.log(err);
    });
}

function capNhatSinhVien() {
  let id = document.getElementById("txtMaSV").value;

  let newSV = layThongTinTuForm();
  batLoading();

  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "PUT",
    data: newSV,
  })
    .then(function (res) {
      tatLoading();
      getDSSV();
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
}

function resetSinhVien() {
  clearForm();

  // let newSV = layThongTinTuForm();
  // batLoading();

  // axios({
  //   url: `${BASE_URL}/sv/`,
  //   method: "PUT",
  //   data: newSV,
  // })
  //   .then(function (res) {
  //     tatLoading();
  //     getDSSV();
  //   })
  //   .catch(function (err) {
  //     tatLoading();
  //     console.log(err);
  //   });
}

// function searchSinhVien() {
//   var name = document.getElementById("txtSearch").value;
//   batLoading();
//   axios({
//     url: `${BASE_URL}/sv`,
//     method: "GET",
//   })
//     .then(function (res) {
//       tatLoading();
//       // console.log(res.data);
//       let dssv = res.data;

//       let index = timKiemViTriTheoTen(name, dssv);
//       let newSvArr = [];
//       newSvArr.push(dssv[index]);
//       renderDSSV(newSvArr);
//     })

//     .catch(function (err) {
//       tatLoading();
//       console.log(err);
//     });
// }

function searchSinhVien() {
  var name = document.getElementById("txtSearch").value;
  batLoading();
  axios({
    url: `${BASE_URL}/sv`,
    method: "GET",
  })
    .then(function (res) {
      tatLoading();
      // console.log(res.data);
      let dssv = res.data;

      let newSvArr = timKiemViTriTheoTen(name, dssv);

      renderDSSV(newSvArr);
    })

    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
}
